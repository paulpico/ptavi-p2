#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys


class Calculadora():

    def __init__(self, value1, value2):
        self.op1 = value1
        self.op2 = value2

    def plus(self):
        """ Function to sum the operands """
        return self.op1 + self.op2

    def minus(self):
        """ Function to substract the operands """
        return self.op1 - self.op2


if __name__ == "__main__":
    try:
        operando1 = float(sys.argv[1])
        operando2 = float(sys.argv[3])
    except ValueError:
        sys.exit("Error: Non numerical parameters")

    calculator = Calculadora(operando1, operando2)

    if sys.argv[2] == "suma":
        result = calculator.plus()
        print("El resultado de la suma es " + str(result))
    elif sys.argv[2] == "resta":
        result = calculator.minus()
        print("El resultado de la resta es " + str(result))
    else:
        sys.exit('Operación sólo puede ser sumar o restar.')
