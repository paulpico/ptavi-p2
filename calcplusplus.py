#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import csv
from calcoohija import CalculadoraHija

# Abrimos el fichero y lo leemos
with open(sys.argv[1], newline='') as File:
    reader = csv.reader(File)
    for row in reader:
        operador = row[0]
        operandos = row[1:]
        result = float(operandos[0])
        for operando in operandos[1:]:
            count = float(operando)
            if operador == "suma":
                result = CalculadoraHija(result, count).plus()
            elif operador == "resta":
                result = CalculadoraHija(result, count).minus()
            elif operador == "multiplica":
                result = CalculadoraHija(result, count).multi()
            elif operador == "divide":
                result = CalculadoraHija(result, count).div()
            else:
                sys.exit('No he encontrado la operacion que pides.')

        print(result)
