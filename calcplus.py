#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from calcoohija import CalculadoraHija

# Abrimos el fichero y lo leemos
file = open(sys.argv[1], "r")

lines = file.readlines()

for line in lines:
    operador = line.split(",")[0]
    operandos = line.split(",")[1:]
    result = float(operandos[0])
    for operando in operandos[1:]:
        count = float(operando)
        if operador == "suma":
            result = CalculadoraHija(result, count).plus()
        elif operador == "resta":
            result = CalculadoraHija(result, count).minus()
        elif operador == "multiplica":
            result = CalculadoraHija(result, count).multi()
        elif operador == "divide":
            result = CalculadoraHija(result, count).div()
        else:
            sys.exit('No he encontrado la operacion que pides.')

    print(result)
