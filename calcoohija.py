#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import calcoo


class CalculadoraHija(calcoo.Calculadora):

    def multi(self):
        """ Function to multiply the operands """
        return self.op1 * self.op2

    def div(self):
        """ Function to divide the operands """
        if self.op2 == 0:
            return "Division by zero is not allowed"
        return self.op1 / self.op2


if __name__ == '__main__':
    try:
        operando1 = float(sys.argv[1])
        operando2 = float(sys.argv[3])
    except ValueError:
        sys.exit("Error: Non numerical parameters")

    calculator2 = CalculadoraHija(operando1, operando2)

    if sys.argv[2] == "suma":
        result = calculator2.plus()
        print("El resultado de la suma es " + str(result))
    elif sys.argv[2] == "resta":
        result = calculator2.minus()
        print("El resultado de la resta es " + str(result))
    elif sys.argv[2] == "multiplica":
        result = calculator2.multi()
        print("El resultado de la multiplicacion es " + str(result))
    elif sys.argv[2] == "divide":
        result = calculator2.div()
        print("El resultado de la division es " + str(result))
    else:
        sys.exit('No he encontrado la operacion que pides.')
